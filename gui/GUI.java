package tanner.gui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;

import org.powerbot.script.rt4.ClientContext;

import tanner.Tanner;

public class GUI extends JFrame {
	
	private Tanner tanner;
	private ClientContext ctx = null;
	
	final String[] leatherOptions = {"Soft Leather", "Hard Leather", "Snakeskin", "SnakeskinAlt",
			"Green d'hide", "Blue d'hide", "Red d'hide", "Black d'hide"};
	
	ArrayList<JButton> leatherButtons = new ArrayList<JButton>();
	
	
	public GUI(ClientContext ctx, Tanner tanner) {
		this.ctx = ctx;
		this.tanner = tanner;
		
		init();
	}
	
	private void init() {
		for (int i = 0; i < leatherOptions.length; i++) {
			final String options = leatherOptions[i];
			final JButton leatherButton = new JButton(options);
			leatherButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					tanner.init(options);
					dispose();
				}
			});
			leatherButtons.add(leatherButton);
		}
		
		setLayout(new FlowLayout());
		
		
		for (JButton btn : leatherButtons) {
			add(btn);
		}
		
		setTitle("OS Ghetto Tanner");
		setSize(300, 200);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				ctx.controller.stop();
				dispose();
			}
		});
		
	}
}