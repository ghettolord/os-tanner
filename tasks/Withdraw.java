package tanner.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.Bank;
import org.powerbot.script.rt4.ClientContext;

import tanner.Tanner;
import tanner.tasks.Task;

public class Withdraw extends Task<ClientContext> {

	public Withdraw(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// withdraw if only coins in inventory & booth in viewport
		return ((ctx.inventory.select().count() == 1)
			&& !ctx.inventory.id(Tanner.COINS_ID).isEmpty()
			&& ctx.objects.select().id(Tanner.BOOTH_ID).nearest().poll().inViewport())
			|| ctx.inventory.select().isEmpty();
	}

	@Override
	public void execute() {
		if (!ctx.bank.opened()) {
			// if bank isn't open, open it.
			ctx.objects.select().id(Tanner.BOOTH_ID).nearest().poll().interact("Bank", "Bank booth");
			
			// continue once bank open
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					return ctx.bank.opened();
				}
			}, 200, 20);
		}
		
		if (ctx.bank.opened()) {
			// if we have less than 600 coins, withdraw designated amount
			if (ctx.inventory.select().id(Tanner.COINS_ID).count(true) < 600) {
				if (ctx.bank.select().id(Tanner.COINS_ID).count(true) < 600) {
					// running out of coins, lets stop.
					ctx.controller.stop();
				}
				ctx.bank.withdraw(Tanner.COINS_ID, Tanner.COINS_AMOUNT);
				// continue once we have the coins
				Condition.wait(new Callable<Boolean>() {				
					@Override
					public Boolean call() {
						return ctx.inventory.select().id(Tanner.COINS_ID).count(true) > 600;
					}
				}, 200, 20);
			}
			
			// check if we have hides
			if (ctx.bank.select().id(Tanner.HIDE_ID).isEmpty()) {
				// no more hides, lets stop.
				ctx.controller.stop();
			}
			
			// withdraw hides
			ctx.bank.withdraw(Tanner.HIDE_ID, Bank.Amount.ALL);
			// continue once we have the hides
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					return !ctx.inventory.select().id(Tanner.HIDE_ID).isEmpty();
				}
			}, 200, 20);
		}
	}

}
