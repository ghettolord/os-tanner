package tanner.tasks;

import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.TilePath;

import tanner.Tanner;

public class GoToBank extends Task<ClientContext> {
	
	private final TilePath pathToBank = ctx.movement.newTilePath(Tanner.TILES_TO_BANK);
	
	public GoToBank(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// if no hide in inventory & booth not in viewport, go to bank
		return ctx.inventory.select().id(Tanner.HIDE_ID).isEmpty()
			&& !ctx.objects.select().id(Tanner.BOOTH_ID).nearest().poll().inViewport();
	}

	@Override
	public void execute() {
		pathToBank.traverse();
	}

}
