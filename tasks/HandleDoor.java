package tanner.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.GameObject;

import tanner.Tanner;

public class HandleDoor extends Task<ClientContext> {

	public HandleDoor(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		return false;
	}
	
	public boolean activate(Task<ClientContext> task) {
		// activate, door closed && (if task is GoToBank && we are inside trader area
		// or if task is TanHides && we are outside trader area)
		if (!ctx.movement.reachable(Tanner.INSIDE_TRADER, Tanner.OUTSIDE_TRADER)) {
			if (task instanceof GoToBank && Tanner.TRADER_AREA.contains(ctx.players.local())) {
				return true;
			} else if (task instanceof TanHides && !Tanner.TRADER_AREA.contains(ctx.players.local())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void execute() {
		GameObject door = ctx.objects.select().id(Tanner.DOOR_ID).within(Tanner.DOOR_AREA).poll();
		door.bounds(136, 114, -214, -2, 18, 115);
		door.interact(false, "Open", "Door");
		
		// Continue once door open	
		Condition.wait(new Callable<Boolean>() {				
			@Override
			public Boolean call() {
				if (ctx.objects.select().id(Tanner.DOOR_ID).within(Tanner.DOOR_AREA).isEmpty()) {
					return true;
				} else {
					return false;
				}
			}
		}, 200, 5);
	}



}
