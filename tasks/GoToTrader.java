package tanner.tasks;

import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.TilePath;

import tanner.Tanner;

public class GoToTrader extends Task<ClientContext> {
	
	private final TilePath pathToTrader = ctx.movement.newTilePath(Tanner.TILES_TO_BANK).reverse();
	
	public GoToTrader(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// if we have hide, money & trader is not in viewport go to trader
		return !ctx.inventory.select().id(Tanner.HIDE_ID).isEmpty()
			&& ctx.inventory.select().id(Tanner.COINS_ID).count(true) >= 600
			&& !ctx.npcs.select().id(Tanner.TRADER_ID).poll().inViewport();
	}

	@Override
	public void execute() {
		pathToTrader.traverse();
	}

}
