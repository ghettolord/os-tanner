package tanner.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.Filter;
import org.powerbot.script.rt4.Bank;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.Inventory;
import org.powerbot.script.rt4.Item;

import tanner.Tanner;

public class Deposit extends Task<ClientContext> {

	public Deposit(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// if other item but coins in inventory & no hide
		final boolean haveCoins = !ctx.inventory.select().id(Tanner.COINS_ID).isEmpty();
		boolean haveOtherStuff;
		if (haveCoins) {
			haveOtherStuff = (ctx.inventory.select().count() > 1);
		} else {
			haveOtherStuff = (ctx.inventory.select().count() >= 1);
		}
		return haveOtherStuff
			&& ctx.inventory.select().id(Tanner.HIDE_ID).isEmpty()
			&& ctx.objects.select().id(Tanner.BOOTH_ID).nearest().poll().inViewport();
	}

	@Override
	public void execute() {
		if (!ctx.bank.opened()) {
			// if bank isn't open, open it.
			ctx.objects.select().id(Tanner.BOOTH_ID).nearest().poll().interact("Bank", "Bank booth");
			
			// continue once bank open
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					return ctx.bank.opened();
				}
			}, 200, 20);
		}
		
		if (ctx.bank.opened()) {
			ctx.inventory.select().select(new Filter<Item>(){
				@Override
				public boolean accept(Item item) {
					return (item.id() != Tanner.COINS_ID);
				}
			});
			
			// banking ALL items but the coins, just in case
			final Inventory inv = ctx.inventory;
			for (Item item : inv) {
				ctx.bank.deposit(item.id(), Bank.Amount.ALL);
				Condition.sleep(300);
				if (!ctx.inventory.select().id(Tanner.COINS_ID).isEmpty() && ctx.inventory.select().count() == 1) break;
			}
			
			// if we somehow got more than 2x the amount of coins we're supposed to carry
			// bank all. Something must have went wrong.
			if (ctx.inventory.select().id(Tanner.COINS_ID).count(true) > (2 * Tanner.COINS_AMOUNT)) {
				ctx.bank.depositInventory();
				Condition.wait(new Callable<Boolean>() {				
					@Override
					public Boolean call() {
						return ctx.inventory.select().count() == 0;
					}
				}, 200, 20);
			}
		}
	}

}
