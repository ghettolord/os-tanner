package tanner.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.Component;

import tanner.Tanner;

public class TanHides extends Task<ClientContext> {

	public TanHides(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// if we have hides, money & trader is in viewport, tann'em
		return !ctx.inventory.select().id(Tanner.HIDE_ID).isEmpty()
			&& ctx.inventory.select().id(Tanner.COINS_ID).count(true) >= 600
			&& ctx.npcs.select().id(Tanner.TRADER_ID).poll().inViewport();
	}

	@Override
	public void execute() {
		final Component tanComponent = ctx.widgets.component(Tanner.WIDGET, Tanner.COMPONENT);
		ctx.npcs.select().id(Tanner.TRADER_ID).poll().interact("Trade", Tanner.TRADER_NAME);
		
		// now check if widget is open
		Condition.wait(new Callable<Boolean>() {
			@Override
			public Boolean call() {
				return tanComponent.visible();
			}
		}, 200, 20);
		
		if (tanComponent.visible()) {
			tanComponent.interact("Tan All");
			
			// continue once tanned
			Condition.wait(new Callable<Boolean>() {
				@Override
				public Boolean call() {
					return ctx.inventory.select().id(Tanner.HIDE_ID).isEmpty();
				}
			}, 200, 20);
			
		}
	}

}
