package tanner;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.powerbot.script.Area;
import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;

import tanner.gui.GUI;
import tanner.tasks.Deposit;
import tanner.tasks.GoToBank;
import tanner.tasks.GoToTrader;
import tanner.tasks.HandleDoor;
import tanner.tasks.TanHides;
import tanner.tasks.Task;
import tanner.tasks.Withdraw;

@Script.Manifest(name = "OS Ghetto Tanner", description = "Tans hides at al kharid.", properties = "client=4;topic=1289711")
public class Tanner extends PollingScript<ClientContext> implements PaintListener, MessageListener {
	// Constants
	public static final Font FONT = new Font("Arial", Font.BOLD, 12);
	
	public static final Tile[] TILES_TO_BANK = {
		new Tile(3273, 3191, 0),
		new Tile(3276, 3191, 0),
		new Tile(3280, 3191, 0),
		new Tile(3281, 3185, 0),
		new Tile(3278, 3181, 0),
		new Tile(3276, 3173, 0),
		new Tile(3274, 3167, 0),
		new Tile(3271, 3167, 0)
	};
	
	public static final Area BANK_AREA = new Area(new Tile(3271, 3170, 0), new Tile(3269, 3164, 0));
	public static final Area TRADER_AREA = new Area(new Tile(3277, 3189, 0), new Tile(3270, 3194, 0));
	public static final Area DOOR_AREA = new Area(new Tile(3279, 3190, 0), new Tile(3276, 3192, 0));
	
	public static final Tile INSIDE_TRADER = new Tile(3275, 3191, 0);
	public static final Tile OUTSIDE_TRADER = new Tile(3281, 3191, 0);
	
	public static final int DOOR_ID = 7122;
	public static final int BOOTH_ID = 11744;
	public static final int COINS_ID = 995;
	public static final int TRADER_ID = 3231;
	public static final int WIDGET = 324;
	public static final int COINS_AMOUNT = 10000;
	
	public static final String TRADER_NAME = "Ellis";
	
	// IDs set by user
	public static int HIDE_ID;
	public static int COMPONENT;
	public static String HIDE_NAME;
	
	// Variables
	private List<Task> taskList = new ArrayList<Task>();
	private boolean startTasks = false;
	private int hidesTanned = 0;
	
	private HandleDoor handleDoor = null;
	
	@Override
	public void start() {
		
		GUI gui = new GUI(ctx, this);
		gui.setVisible(true);
		
		handleDoor = new HandleDoor(ctx);
		taskList.addAll(Arrays.asList(new GoToTrader(ctx), new TanHides(ctx), new GoToBank(ctx), new Deposit(ctx), new Withdraw(ctx)));
	}
	
	@Override
	public void poll() {
		if (startTasks) {
			for (Task<ClientContext> task : taskList) {
				if (task.activate()) {
					if (handleDoor.activate(task)) {
						handleDoor.execute();
					} 
					if (!handleDoor.activate(task)) {
						task.execute();
					}
				}
			}
		}
	}
	
	public void init(String options) {
		if (options == "Soft Leather") {
			HIDE_ID = 1739; // Cow Hide
			COMPONENT = 100;
		} else if (options == "Hard Leather") {
			HIDE_ID = 1739; // Cow Hide
			COMPONENT = 101;
		} else if (options == "Snakeskin") {
			HIDE_ID = 7801; // Snake hide
			COMPONENT = 102;
		} else if (options == "SnakeskinAlt") {
			HIDE_ID = 6287; // Snake hide?
			COMPONENT = 103;
		} else if (options == "Green d'hide") {
			HIDE_ID = 1753; // Green dragonhide
			COMPONENT = 104;
		} else if (options == "Blue d'hide") {
			HIDE_ID = 1751; // Blue dragonhide
			COMPONENT = 105;
		} else if (options == "Red d'hide") {
			HIDE_ID = 1749; // Red dragonhide
			COMPONENT = 106;
		} else if (options == "Black d'hide") {
			HIDE_ID = 1747; // Black dragonhide
			COMPONENT = 107;
		}
		startTasks = true;
	}
	
	@Override
	public void repaint(Graphics graphics) {
		final Graphics2D g = (Graphics2D) graphics;
		g.setFont(FONT);
		
		final long currentRuntime = getRuntime();
		final long second = (currentRuntime / 1000) % 60;
		final long minute = (currentRuntime / (1000 * 60)) % 60;
		final long hour = (currentRuntime / (1000 * 60 * 60)) % 24;
		final String duration = String.format("Duration: %02d:%02d:%02d", hour, minute, second);
		
		final int hidesHr = (int) ((hidesTanned * 3600000D) / getRuntime());
		
		g.setColor(Color.BLACK);
		g.fillRect(5, 5, 200, 60);
		
		g.setColor(Color.PINK);
		g.drawString("Ghetto Tanner", 10, 20);
		g.setColor(Color.WHITE);
		g.drawString(duration, 10, 40);
		g.drawString(String.format("Tanned: %,d (%,d/hr)", hidesTanned, hidesHr), 10, 60);
	}

	@Override
	public void messaged(MessageEvent e) {
		final String msg = e.text();
		if(msg.startsWith("The tanner tans ") && msg.endsWith(" cowhides for you.")) {
			hidesTanned += Integer.parseInt(msg.split(" ")[3]);
		}
	}
	
}
